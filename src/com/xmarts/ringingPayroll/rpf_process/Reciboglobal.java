 /*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.0  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SL
 * All portions are Copyright (C) 2001-2006 Openbravo SL
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.xmarts.ringingPayroll.rpf_process;

import org.openbravo.erpCommon.utility.*;
import org.openbravo.erpCommon.businessUtility.Tax;
import org.openbravo.erpCommon.businessUtility.Tree;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.base.secureApp.*;
import org.openbravo.xmlEngine.XmlDocument;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.openbravo.utils.Replace;
import java.math.BigDecimal;
import java.lang.Object;
import java.text.*;
import java.sql.*;
import org.openbravo.base.filter.RequestFilter;
import org.openbravo.base.filter.ValueListFilter;
import org.openbravo.data.FieldProvider;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;

// imports for transactions
import java.sql.Connection;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

public class Reciboglobal extends HttpSecureAppServlet {
  static final BigDecimal ZERO = new BigDecimal(0.0);
  String strVentanaId ="";

  public void init (ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
  
    VariablesSecureApp vars = new VariablesSecureApp(request);
    if (vars.commandIn("DEFAULT", "DIRECT")) { 

    String sessionValuePrefix = "RECIBOGLOBAL";
    String strKey = null;
    strKey = vars.getSessionValue(sessionValuePrefix + ".inprpfPayrollId_R");
    System.out.println("Reciboglobal ID "+strKey);
    if (strKey.equals("")){
        strKey = vars.getSessionValue(sessionValuePrefix + ".inprpfPayrollId");
        System.out.println("Reciboglobal ID2 "+strKey);
    }

       strKey = Replace.replace(strKey, "'", "");
       strKey = Replace.replace(strKey, "(", "");
       strKey = Replace.replace(strKey, ")", "");
       System.out.println("ID "+strKey);
       printPagePartePDF(response, vars,strKey);
     
      }
    }

  void printPagePartePDF(HttpServletResponse response, VariablesSecureApp vars, String strKey) throws IOException,ServletException{
  
    if (log4j.isDebugEnabled()) log4j.debug("Output: RptC_ReciboGlobal - pdf");
               JasperPrint jasperPrint;
               String strReportName="";
               HashMap<String, Object> parameters = new HashMap<String, Object>();
               strReportName = "@basedesign@/com/xmarts/ringingPayroll/rpf_process/report1.jrxml";
               response.setHeader("Content-disposition", "inline; filename=Reciboglobal.pdf");
	             parameters.put("DOCUMENT_ID",strKey);
               parameters.put("BASE_DESIGN","@basedesign@");
               renderJR(vars, response, strReportName, "pdf", parameters,null, null );
               System.out.println("Termino de imprimir ");      

  }

  
}
