	/************************************************************************************ 
 * Copyright (C) 2009 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 ************************************************************************************/

package com.xmarts.ringingPayroll.rpf_process;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import java.lang.Exception; 

import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.security.PrivateKey;
import java.security.Signature;
import org.apache.commons.ssl.PKCS8Key;
import java.io.BufferedInputStream;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.security.GeneralSecurityException; 

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Connection;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.xmlEngine.XmlDocument;


//LIBRERIAS NUEVA FORMA TIMBRADO
import com.solucionfactible.ws.timbrado.client.*;
import com.solucionfactible.cfdi.ws.timbrado.xsd.*;


//Libreria desencriptado
import org.openbravo.utils.*;


public class Canceled extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  //private FileGeneration fileGeneration = new FileGeneration();

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Posted: doPost");

    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strKey = vars.getGlobalVariable("inprpfPayrollTimbreId", "Canceled|RPF_Payroll_Timbre_ID");
      System.out.println("Clave Default:" + strKey);
      String strWindowId = vars.getGlobalVariable("inpwindowId", "Canceled|windowId");
      String strTabId = vars.getGlobalVariable("inpTabId", "Canceled|tabId");
      String strProcessId = vars.getGlobalVariable("inpProcessed", "Canceled|processId", "");
      String strPath = vars.getGlobalVariable("inpPath", "Canceled|path", strDireccion
          + request.getServletPath());
      String strPosted = null;
      String strRingd = "N";
      String strDocNo = vars.getRequestGlobalVariable("inpdocumentno", "Canceled|docNo");
      String strWindowPath = Utility.getTabURL(this, strTabId, "E");
      if (strWindowPath.equals(""))
        strWindowPath = strDefaultServlet; 

      if (strRingd.equals("Y"))
	advisePopUp(request, response, Utility.messageBD(this, "ERROR", vars.getLanguage()),
	    Utility.messageBD(this, "CFDI_Ring", vars.getLanguage()));

      printPage(response, vars, strKey, strWindowId, strTabId, strProcessId, strPath, strPosted,
          strWindowPath, strDocNo);

    } else if (vars.commandIn("SAVE")) {

      String strKey = vars.getGlobalVariable("inprpfPayrollTimbreId", "Canceled|RPF_Payroll_Timbre_ID");
      String strWindowId = vars.getRequestGlobalVariable("inpwindowId", "Canceled|windowId");
      String strTabId = vars.getRequestGlobalVariable("inpTabId", "Canceled|tabId");
      String strProcessId = vars.getRequestGlobalVariable("inpProcessed", "Canceled|processId");
      String strPath = vars.getRequestGlobalVariable("inpPath", "Canceled|path");
      String strPosted = null;
      String strDocNo = vars.getRequestGlobalVariable("inpdocumentno", "Canceled|docNo");
      String strWindowPath = Utility.getTabURL(this, strTabId, "E");
      String strFilePath = vars.getRequestGlobalVariable("inpFilePath", "Canceled|FileName");

      if (strWindowPath.equals(""))
        strWindowPath = strDefaultServlet;

        OBError myMessage = process(response, vars, strKey, strWindowId, strTabId, strProcessId,
        strPath, strPosted, strWindowPath, strDocNo);
        vars.setMessage(strTabId, myMessage);
	printPageClosePopUp(response, vars, strWindowPath);
     
    } else {
      advisePopUp(request, response, Utility.messageBD(this, "ERROR", vars.getLanguage()), Utility
          .messageBD(this, "ERROR", vars.getLanguage()));
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId, String strPath, String strPosted,
      String strFilePath, String strDocNo) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: Button process Ring Electronic Invoice");
    //System.out.println("metodo prong");
    String[] discard = { "" };
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/xmarts/ringingPayroll/rpf_process/Canceled", discard).createXmlDocument();
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("tab", strTab);
    xmlDocument.setParameter("processed", strProcessId);
    xmlDocument.setParameter("path", strPath);
    xmlDocument.setParameter("posted", strPosted);
    xmlDocument.setParameter("docNo", strDocNo);

    {
      OBError myMessage = vars.getMessage("Canceled");
      vars.removeMessage("Canceled");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("rpfPayrollId", strKey);

    if (!strFilePath.equals("")) {
      xmlDocument.setParameter("inpFilePath", strFilePath);
      xmlDocument.setParameter("inpOnLoad", "onLoadClose();");
    }
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private OBError process(HttpServletResponse response, VariablesSecureApp vars, String strKey,String windowId, String strTab, String strProcessId, String strPath, String strPosted,
      String strWindowPath, String strDocNo) {
     OBError myMessage = new OBError();
     myMessage.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
     System.out.println("Entro al metodo:" + strKey);

     try {
     CanceledData[] data = CanceledData.select(this,strKey);

      String rutaKey=data[0].rutakey;
      String rutaCer=data[0].rutacer;
      String strDireccionG = globalParameters.strFTPDirectory;
      System.out.println("Direccion: " + strDireccionG);
      String rutaKey1E=strDireccionG +rutaKey;
      String rutaCer1E=strDireccionG +rutaCer;
      String contraKey=CryptoUtility.decrypt(data[0].clavekey);
      String UsuarioWS=data[0].usuariows;
      String KeyWS=CryptoUtility.decrypt(data[0].clavews);
      String Documento = data[0].documentno;
      String uuid = data[0].uuid;

      String[] uuidE = {uuid};

      try{
        Timbrado timbra = new Timbrado();
        CFDICancelacion cert2 = new CFDICancelacion();         
       
       
        
         cert2 = timbra.cancelar(UsuarioWS, KeyWS,uuidE.clone(),rutaCer1E, rutaKey1E, contraKey);
         System.out.println("Mensaje : " + cert2.getMensaje());

         if(cert2.getMensaje().contains("Autenticación correcta")){
              Connection conn = null;
              try {
              System.out.println("Realizando el guardado de informacion"); 
              conn = this.getTransactionConnection();

              CanceledData.actu(conn,this,strKey);

              System.out.println("Termino el guardado de informacion");
              releaseCommitConnection(conn);
              } catch (Exception e) {
              try {
              releaseRollbackConnection(conn);
              } catch (Exception ignored) {}
              e.printStackTrace();
              log4j.warn("Rollback in transaction");
              myMessage = Utility.translateError(this, vars, vars.getLanguage(), "ProcessRunError");
              System.out.println("Error MyMessage" + myMessage);
              }

        myMessage.setType("Success");
        myMessage.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
        } else {
            myMessage = Utility.translateError(this, vars, vars.getLanguage(), cert2.getMensaje());
         }
       }catch(Exception e){
         System.out.println("Error" + e);
         String error = e.toString();
         System.out.println("Mensaje error: " + error);
       } 



      } catch (ServletException e) {
        e.printStackTrace();
        myMessage.setMessage(Utility.messageBD(this, "FileCannotCreate", vars.getLanguage()));
        return myMessage;
      } 


    return myMessage;
  }







}

