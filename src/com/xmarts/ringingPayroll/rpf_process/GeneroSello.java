/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xmarts.ringingPayroll.rpf_process;

import java.io.*;
import java.io.File;
import java.io.InputStream;
import org.apache.commons.ssl.PKCS8Key; 
import java.security.GeneralSecurityException; 
import java.security.PrivateKey; 
import java.security.Signature; 
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.SignatureException;
import sun.misc.BASE64Encoder; 

/**
 *
 * @author xmarts
 */
public class GeneroSello {
    
    public static String selloD(String cadenaOriginal, String ruta, String contrasena){

  System.out.println("Cadena Original clase sello: " + cadenaOriginal);
  String selloDigital="";
  String pass=contrasena; //original
// String pass = "12345678a"; //prueba
  try{
    // LEE EL ARCHIVO
    FileInputStream fis = new FileInputStream(ruta); //prueba
    //FileInputStream fis = new FileInputStream("/opt/ase081010612_1107121343s.key"); original
       System.out.println(fis);
     byte[] clavePrivada = getBytes(fis); 
      System.out.println(clavePrivada);

    PKCS8Key pkcs8  = null;
    try{
      pkcs8  =  new PKCS8Key(clavePrivada, pass.toCharArray()); 
        
    }catch (GeneralSecurityException ex) { 
        ex.printStackTrace();
        System.out.println("No es el pass de la llave no es coreecto");
     }

    PrivateKey pk = pkcs8.getPrivateKey(); 
      System.out.println(pk);
    Signature firma = null;

    try{
      firma = Signature.getInstance("SHA1withRSA");
      firma.initSign(pk); 
      try {
         try{
            //GENERA SELLO DIGITAL
            firma.update(cadenaOriginal.getBytes("UTF-8"));
            BASE64Encoder b64 = new BASE64Encoder();
            selloDigital = b64.encode(firma.sign());
         } catch (SignatureException e){
           }

       } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
         }
         System.out.println("SELLO DIGITAL :  "+ selloDigital); 

        }catch (NoSuchAlgorithmException ex){
   }catch (InvalidKeyException ex){
  }
 } catch (IOException e) {
  e.printStackTrace();
} 

 return selloDigital;
}



 public static byte[] getBytes(FileInputStream is) {
  int totalBytes = 1024;
  byte[] buffer = null;
    try {
     buffer = new byte[totalBytes];
     is.read(buffer, 0, totalBytes);
       } catch (IOException e) {
               e.printStackTrace();
             }
    return buffer;
 }
}
