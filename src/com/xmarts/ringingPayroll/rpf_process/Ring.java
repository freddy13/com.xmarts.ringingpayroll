	/************************************************************************************ 
 * Copyright (C) 2009 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 ************************************************************************************/

package com.xmarts.ringingPayroll.rpf_process;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import java.lang.Exception; 

import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.security.PrivateKey;
import java.security.Signature;
import org.apache.commons.ssl.PKCS8Key;
import java.io.BufferedInputStream;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.security.GeneralSecurityException; 

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Connection;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.xmlEngine.XmlDocument;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.dal.core.OBContext;
//LIBRERIAS FACTURA ELECTRONICA
import org.apache.axis.client.Call; 
import java.text.DateFormat;
import java.util.Date;
import org.apache.axis.client.Service; 
import org.apache.axis.encoding.XMLType; 
import java.util.Calendar;
import java.math.BigDecimal;
import mx.gob.sat.www.cfd._3.*;
import mx.gob.sat.www.TimbreFiscalDigital.*;
import org.tempuri.*;
import com.ibm.wsdl.extensions.soap.SOAPConstants; 
import java.text.SimpleDateFormat;
import javax.xml.rpc.ParameterMode; 
import org.openbravo.database.ConnectionProvider;

//LIBRERIAS NUEVA FORMA TIMBRADO
import com.solucionfactible.ws.timbrado.client.*;
import com.solucionfactible.cfdi.ws.timbrado.xsd.*;
//LIBRERIAS CADENAORIGINAL
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
//LIBRERIAS SELLADO
import org.jdom2.Document;  
import org.jdom2.Element;  
import org.jdom2.JDOMException;  
import org.jdom2.Namespace;  
import org.jdom2.input.SAXBuilder;  
import org.jdom2.output.Format;  
import org.jdom2.output.XMLOutputter;  
import org.jdom2.xpath.XPath;  

import java.awt.*;

//Libreria desencriptado
import org.openbravo.utils.*;


public class Ring extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  //private FileGeneration fileGeneration = new FileGeneration();

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Posted: doPost");

    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strKey = vars.getGlobalVariable("inprpfPayrollId", "Ring|RPF_Payroll_ID");
      System.out.println("Clave Default:" + strKey);
      String strWindowId = vars.getGlobalVariable("inpwindowId", "Ring|windowId");
      String strTabId = vars.getGlobalVariable("inpTabId", "Ring|tabId");
      String strProcessId = vars.getGlobalVariable("inpProcessed", "Ring|processId", "");
      String strPath = vars.getGlobalVariable("inpPath", "Ring|path", strDireccion
          + request.getServletPath());
      String strPosted = null;
      String strRingd = "N";
      String strDocNo = vars.getRequestGlobalVariable("inpdocumentno", "Ring|docNo");
      String strWindowPath = Utility.getTabURL(this, strTabId, "E");
      if (strWindowPath.equals(""))
        strWindowPath = strDefaultServlet; 

      if (strRingd.equals("Y"))
	advisePopUp(request, response, Utility.messageBD(this, "ERROR", vars.getLanguage()),
	    Utility.messageBD(this, "CFDI_Ring", vars.getLanguage()));

      printPage(response, vars, strKey, strWindowId, strTabId, strProcessId, strPath, strPosted,
          strWindowPath, strDocNo);

    } else if (vars.commandIn("SAVE")) {

      String strKey = vars.getGlobalVariable("inprpfPayrollId", "Ring|RPF_Payroll_ID");
      String strWindowId = vars.getRequestGlobalVariable("inpwindowId", "Ring|windowId");
      String strTabId = vars.getRequestGlobalVariable("inpTabId", "Ring|tabId");
      String strProcessId = vars.getRequestGlobalVariable("inpProcessed", "Ring|processId");
      String strPath = vars.getRequestGlobalVariable("inpPath", "Ring|path");
      String strPosted = null;
      String strDocNo = vars.getRequestGlobalVariable("inpdocumentno", "Ring|docNo");
      String strWindowPath = Utility.getTabURL(this, strTabId, "E");
      String strFilePath = vars.getRequestGlobalVariable("inpFilePath", "Ring|FileName");

      if (strWindowPath.equals(""))
        strWindowPath = strDefaultServlet;

        OBError myMessage = process(response, vars, strKey, strWindowId, strTabId, strProcessId,
        strPath, strPosted, strWindowPath, strDocNo);
        vars.setMessage(strTabId, myMessage);
	printPageClosePopUp(response, vars, strWindowPath);
     
    } else {
      advisePopUp(request, response, Utility.messageBD(this, "ERROR", vars.getLanguage()), Utility
          .messageBD(this, "ERROR", vars.getLanguage()));
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId, String strPath, String strPosted,
      String strFilePath, String strDocNo) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: Button process Ring Electronic Invoice");
    //System.out.println("metodo prong");
    String[] discard = { "" };
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/xmarts/ringingPayroll/rpf_process/Ring", discard).createXmlDocument();
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("tab", strTab);
    xmlDocument.setParameter("processed", strProcessId);
    xmlDocument.setParameter("path", strPath);
    xmlDocument.setParameter("posted", strPosted);
    xmlDocument.setParameter("docNo", strDocNo);

    {
      OBError myMessage = vars.getMessage("Ring");
      vars.removeMessage("Ring");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("rpfPayrollId", strKey);

    if (!strFilePath.equals("")) {
      xmlDocument.setParameter("inpFilePath", strFilePath);
      xmlDocument.setParameter("inpOnLoad", "onLoadClose();");
    }
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private OBError process(HttpServletResponse response, VariablesSecureApp vars, String strKey,String windowId, String strTab, String strProcessId, String strPath, String strPosted,
      String strWindowPath, String strDocNo) {
     OBError myMessage = new OBError();
     myMessage.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
     

      try{
      EmpData[] emp = EmpData.emple(this,strKey);

          for(int x=0;x<emp.length;x++){

          /****FECHA DEL TIMBRADO ************/
          java.util.Date strfecha = new Date();
          SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
          String fecha = format1.format(strfecha);
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
          String fechatim = sdf.format(strfecha);
          try{
          String id =emp[x].rpfPayrollTimbreId;
          System.out.println("entro al metodo :p" + id);
          String g=id;
          String r=null;
          for(int x1=2;x1<g.length();x1=x1+3){
           if(x1==2){
           r=""+g.charAt(x1-2)+g.charAt(x1-1)+g.charAt(x1);   
           }else{
           r=r+"/"+g.charAt(x1-2)+g.charAt(x1-1)+g.charAt(x1);
           }
           if(x1==29){
           r=r+"/"+g.charAt(x1+1)+g.charAt(x1+2)+"/";   
           }
          }
            //System.out.println("Ruta del id divido "+r);
        
            String strDireccion = globalParameters.strFTPDirectory+"/"+"DEBECB0C59D34B3C80F5578FB622AB14/"+r;
        
            System.out.println("Direccion del attachments "+strDireccion);

            File uploadedDir = new File(strDireccion);
             if (!uploadedDir.exists())
             uploadedDir.mkdirs();


          RnigData[] data = RnigData.select(this,id);
          AddiData[] add = AddiData.addic(this,id);
          DeducData[] ded = DeducData.deduc(this,id);

          double toadd = 0;
          for(int a=0;a<add.length;a++){
               System.out.println("Entro a las adicciones "+add[a].mnt.replaceAll(",",""));
               double tmp = Double.parseDouble(add[a].mnt.replaceAll(",",""));
               toadd = toadd + tmp;
          }
          System.out.println("SAlio el for");
          int cifras=(int) Math.pow(10,6);
          toadd = Math.rint(toadd*cifras)/cifras;
          System.out.println("Total addiciones "+toadd);

          double toded = 0;
          for(int d=0;d<ded.length;d++){
               double tmp = Double.parseDouble(ded[d].monto.replaceAll(",",""));
               toded = toded + tmp;
          }
          toded = Math.rint(toded*cifras)/cifras;
          System.out.println("Total deducciones "+toded);

          Double strsub = toadd;
          strsub= Math.rint(strsub*cifras)/cifras;
          System.out.println("Subtotal "+strsub);
          Double strtotal = toadd - toded;
          strtotal = Math.rint(strtotal*cifras)/cifras;
          System.out.println("Total "+strtotal);
          /*****************************************************************CLAVES SAT********************/
          String tiporegR = null;
          if(data[0].tiporegimen.equals("SUELDOS Y SALARIOS")){
          tiporegR = "2";
          }else{
          tiporegR = data[0].tiporegimen;
          }
          String banco = null;
          if(data[0].banco.equals("BANORTE")){
          banco = "072";
          }else{
            if(data[0].banco.equals("BANAMEX")){
            banco = "002";
            }else{
            banco = data[0].banco;
            }
          }
          String riesgopuesto = null;
          if(data[0].riesgo.equals("2.65325")){
          riesgopuesto = "2";
          }else{
            if(data[0].riesgo.equals("1,653")){
            riesgopuesto = "1";
            }else{
            riesgopuesto = data[0].riesgo;
            }
          }
          String noint = null;
          if(data[0].nointrec.equals("")){
          noint = "";
          }else{
          noint = "noInterior=\""+data[0].nointrec+"\"";
          }
          StringBuffer StrXML = new StringBuffer();
          PrintStream xml = new PrintStream(strDireccion+"Nomina"+data[0].folio+".xml", "UTF-8");
          StrXML.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
          StrXML.append("<cfdi:Comprobante xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/nomina http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina11.xsd\"\n" +"  xmlns:nomina=\"http://www.sat.gob.mx/nomina\"\n"+"  xmlns:cfdi=\"http://www.sat.gob.mx/cfd/3\" \n" +
          "  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"3.2\"  serie=\"NOM\"  folio=\""+data[0].folio+"\" fecha=\""+fechatim+"\" tipoDeComprobante=\"egreso\" formaDePago=\""+data[0].formapago+"\" condicionesDePago=\""+data[0].condiciones+"\" metodoDePago=\""+ data[0].metodopago+"\" noCertificado=\""+data[0].nocertificado+"\"  subTotal=\""+strtotal+"\" Moneda=\""+data[0].moneda+"\" total=\""+strtotal+"\" LugarExpedicion=\""+data[0].lugarexp+"\" sello=\" \" certificado=\""+data[0].certificado+"\">");
           
           //Emisor
           StrXML.append("\n<cfdi:Emisor rfc=\""+data[0].rfcem.replace("&", "&amp;")+"\" nombre=\""+ data[0].nombreemi.replace("&", "&amp;") +"\" >");
           StrXML.append("\n<cfdi:DomicilioFiscal calle=\""+data[0].calleemi+"\" noExterior=\""+data[0].noextemi+"\" colonia=\""+data[0].coloniaemi+"\" localidad=\""+data[0].localidademi+"\" municipio=\""+data[0].munucipioemi+"\" estado=\""+data[0].estadoemi+"\" pais=\"Mexico\" codigoPostal=\""+data[0].cpemi+"\"/>");
           StrXML.append("\n<cfdi:ExpedidoEn calle=\""+data[0].calleemi+"\" noExterior=\""+data[0].noextemi+"\" colonia=\""+data[0].coloniaemi+"\" localidad=\""+data[0].localidademi+"\" municipio=\""+data[0].munucipioemi+"\" estado=\""+data[0].estadoemi+"\" pais=\"Mexico\" codigoPostal=\""+data[0].cpemi+"\"/>");
           StrXML.append("\n<cfdi:RegimenFiscal Regimen=\""+data[0].regimen+"\" />");
           StrXML.append("\n</cfdi:Emisor>");
           
           //Receptor
            StrXML.append("\n<cfdi:Receptor rfc=\""+data[0].rfcrec.replace("555&", "&amp;")+"\" nombre=\""+data[0].nombre.replace("&", "&amp;")+"\">");
            StrXML.append("\n<cfdi:Domicilio calle=\""+data[0].callerec+"\" noExterior=\""+data[0].noextrec+"\" "+noint+" colonia=\""+data[0].coloniarec+"\" localidad=\""+data[0].localidadrec+"\" municipio=\""+data[0].munucipiorec+"\" estado=\""+data[0].estadorec+"\" pais=\"Mexico\"/>");
            StrXML.append("\n</cfdi:Receptor>");
            
            //Concepto
            StrXML.append("<cfdi:Conceptos>\n");
            StrXML.append("<cfdi:Concepto cantidad=\"1\" unidad=\"Servicio\" descripcion=\"Quincena\" valorUnitario=\""+strtotal+"\" importe=\""+strtotal+"\"/>\n");
            StrXML.append("</cfdi:Conceptos>");
            String cat = "0";
            for(int d=0;d<ded.length;d++){
            String cla = ded[d].concepto.substring(0, 3);     
              if(cla.equals("036")){
                  cat = ded[d].monto;
              }
            }
            StrXML.append("\n<cfdi:Impuestos totalImpuestosRetenidos=\""+cat+"\" totalImpuestosTrasladados=\"0.00\">\n <cfdi:Retenciones>\n");
            StrXML.append("<cfdi:Retencion impuesto=\"ISR\" importe=\""+cat+"\" />\n");  
            StrXML.append(" </cfdi:Retenciones>\n </cfdi:Impuestos>\n");
            StrXML.append("<cfdi:Complemento>");
            
            StrXML.append("<nomina:Nomina "+ 
            "Version=\"1.1\" RegistroPatronal=\""+data[0].registropat+"\" NumEmpleado=\""+data[0].numempleado+"\" CURP=\""+data[0].curp+"\" TipoRegimen=\""+tiporegR+"\"\n" +
            "NumSeguridadSocial=\""+data[0].numseguro+"\" FechaPago=\""+data[0].datepayment+"\" FechaInicialPago=\""+data[0].fechaini+"\" FechaFinalPago=\""+data[0].fechafin+"\"\n" +
            "NumDiasPagados=\""+data[0].numdias+"\" Departamento=\""+data[0].depa+"\"  Banco=\""+banco+"\" FechaInicioRelLaboral=\""+data[0].fechainicio+"\"\n" +
            "Antiguedad=\""+data[0].antigue+"\" Puesto=\""+data[0].puesto+"\" TipoContrato=\""+data[0].tipocon+"\" TipoJornada=\""+data[0].tipojor+"\" PeriodicidadPago=\""+data[0].periocidad+"\"\n" +
            "SalarioBaseCotApor=\""+data[0].salariobase.replace(",", ".")+"\" RiesgoPuesto=\""+riesgopuesto+"\" SalarioDiarioIntegrado=\""+data[0].salariodiario.replace(",", ".")+"\">");
            double exc1=0;
            double grav1=0;
            for(int a=0;a<add.length;a++){
              String cla = add[a].concep.substring(0, 3);
              double tmp = Double.parseDouble(add[a].mnt.replaceAll(",",""));
              if(cla.equals("201") || cla.equals("598")){
                exc1 = exc1 + tmp;
                }else{
                grav1 = grav1+ tmp;
                }

            }
            exc1 = Math.rint(exc1*cifras)/cifras;
            grav1 = Math.rint(grav1*cifras)/cifras;
            StrXML.append("\n<nomina:Percepciones TotalGravado=\""+grav1+"\" TotalExento=\""+exc1+"\">\n");
              String clave = null;
              for(int a=0;a<add.length;a++){
                String cla = add[a].concep.substring(0, 3);
                clave = cla;
                if(cla.equals("1") || cla.equals("599") || cla.equals("200") || cla.equals("590") || cla.equals("001")){
                    clave = "001";
                }
                if(cla.equals("201")){
                    clave = "021";
                }
                if(cla.equals("211")){
                    clave = "002";
                }
                if(cla.equals("598")){
                    clave = "017";
                }
                if(cla.equals("302")){
                    clave = "003";
                }
                String grav = "0";
                String exc = "0";
                if(cla.equals("201") || cla.equals("598")){
                grav = "0";
                exc = add[a].mnt.replaceAll(",","");
                }else{
                grav = add[a].mnt.replaceAll(",","");
                exc = "0";
                }
          StrXML.append("<nomina:Percepcion TipoPercepcion=\""+clave+"\" Clave=\""+clave+"\" Concepto=\""+add[a].concep+"\" ImporteGravado=\""+grav+"\" ImporteExento=\""+exc+"\"/>\n");
          }
          StrXML.append("</nomina:Percepciones>\n");

          exc1=0;
          grav1=0;
          for(int d=0;d<ded.length;d++){
            String cla = ded[d].concepto.substring(0, 3);     
            double tmp = Double.parseDouble(ded[d].monto.replaceAll(",",""));
            if(cla.equals("800") || cla.equals("801") || cla.equals("904") || cla.equals("901")){
                exc1 = exc1 + tmp;
            }else{
                grav1 = grav1 + tmp;
            }
          }
          exc1 = Math.rint(exc1*cifras)/cifras;
          grav1 = Math.rint(grav1*cifras)/cifras;
          StrXML.append("<nomina:Deducciones TotalGravado=\""+grav1+"\" TotalExento=\""+exc1+"\">\n");
          clave = null;
          for(int d=0;d<ded.length;d++){
              String cla = ded[d].concepto.substring(0, 3);
              clave = cla;
                          if(cla.equals("800")){
                              clave = "002";
                          }
                          if(cla.equals("801")){
                              clave = "001";
                          }
                          if(cla.equals("610") || cla.equals("890") || cla.equals("600")){
                              clave = "020";
                          }
                          if(cla.equals("620")){
                              clave = "006";
                          }
                          if(cla.equals("901")){
                              clave = "004";
                          }
                          if(cla.equals("904")){
                              clave = "010";
                          }
                          String grav = "0";
                          String exc = "0";
                          if(cla.equals("800") || cla.equals("801") || cla.equals("904") || cla.equals("901")){
                          grav = "0";
                          exc = ded[d].monto.replaceAll(",","");
                          }else{
                          grav = ded[d].monto.replaceAll(",","");
                          exc = "0";
                          } 
          StrXML.append("<nomina:Deduccion TipoDeduccion=\""+clave+"\" Clave=\""+clave+"\" Concepto=\""+ded[d].concepto+"\" ImporteGravado=\""+grav+"\" ImporteExento=\""+exc+"\"/>\n");
                  }        
          StrXML.append("</nomina:Deducciones>\n"); 
          StrXML.append(" </nomina:Nomina>\n"+"</cfdi:Complemento>\n" +" </cfdi:Comprobante>");
          xml.println(new String(StrXML.toString().getBytes("UTF-8")));
          String strXml = StrXML.toString();

         String strDirect = globalParameters.strFTPDirectory+"/";
         String strDirXslt = strDirect + "../modules/com.xmarts.ringingPayroll/src/com/xmarts/ringingPayroll/rpf_process/cadenaoriginal_3_2.xslt";
         System.out.println("Ruta del xstl "+strDirXslt);
         String strCadenaOriginal =null;
         try{
             StreamSource sourceXSL = new StreamSource(new File(strDirXslt));
             StreamSource sourceXML = new StreamSource(new File(strDireccion+"Nomina"+data[0].folio+".xml"));
 
             TransformerFactory tFactory = TransformerFactory.newInstance();
             Transformer transformer = tFactory.newTransformer(sourceXSL);
 
             OutputStream output = new ByteArrayOutputStream();
             transformer.transform(sourceXML, new StreamResult(output));
             strCadenaOriginal = new String(((ByteArrayOutputStream) output).toByteArray());
              strCadenaOriginal = strCadenaOriginal.replaceAll("[\n\r]","");
             //System.out.println("Cedena Original de archivo: " + strCadenaOriginal);
    
          }catch(Exception e){
           System.out.println("Resultadoss " + e);
          }
          /***************************************************************************Sellado de nomina****************/
          String rutakey = RnigData.key(this,data[0].orgid);
          if(rutakey.equals("no ruta")){
            myMessage.setMessage("No se encontro el archivo .key");
            myMessage.setType("Error");
            myMessage.setTitle(Utility.messageBD(new DalConnectionProvider(), "Error", OBContext.getOBContext().getLanguage().getLanguage()));
            return myMessage;   
          }
          String ruta=globalParameters.strFTPDirectory +rutakey; 
          String contra=CryptoUtility.decrypt(data[0].clavekey);

          String SelloDig = GeneroSello.selloD(strCadenaOriginal.replaceAll("[\n\r]",""), ruta, contra);
          //System.out.println("SELLO DIGITAL DE LA FACTURA :  " + SelloDig);
             try {
          SAXBuilder builder = new SAXBuilder ();
          Document doc = builder.build (new FileInputStream (strDireccion+"Nomina"+data[0].folio+".xml"));
           
          Element raiz = doc.getRootElement();
          raiz.setAttribute("sello", SelloDig.replaceAll("[\n\r]",""));       
          XMLOutputter outputter = new XMLOutputter();
          FileWriter writer = new FileWriter(strDireccion+"Nomina"+data[0].folio+".xml");
          outputter.output(doc, writer);
          writer.close(); 
          /***************************************************************************Timbrado de nomina****************/
          String usuario_wb=data[0].usuariows;
          String pass_wb=CryptoUtility.decrypt(data[0].clavews);
          boolean production=true;
          String prue = RnigData.pruebas(this,data[0].orgid);
         /*System.out.println("Usuario "+usuario_wb);
          System.out.println("Contrasena "+pass_wb);*/
          System.out.println("Resultado del query para Pruebas "+prue);
          if(prue.equals("Y")){
          production=false;
          }
          System.out.println("Pruebas "+production);
         // String archivo = strDireccion+"Nomina"+data[0].folio+".xml";
          String strErrorE = null;
          try{ 
         // INICIA CODIGO QUE MANDA A TIMBRAR EL XML
          Timbrado timbra = new Timbrado();
          CFDICertificacion cert = new CFDICertificacion(); 
          System.out.println("Timbrando "+strDireccion+"Nomina"+data[0].folio+".xml");
          cert = timbra.timbrar(usuario_wb, pass_wb, strDireccion+"Nomina"+data[0].folio+".xml", production);
          System.out.println("Mensaje webservice "+cert.getResultados()[0].getMensaje());
          strErrorE = cert.getResultados()[0].getMensaje();
          if(cert.getResultados()[0].getMensaje().equals("La operación se completó con éxito.")){
          //System.out.println("Entro al if ");
          Calendar vFechaTimbrado = cert.getResultados()[0].getFechaTimbrado();
          //System.out.println("Fecha de timbrado "+vFechaTimbrado);
          Date vFechaTimbradoW=vFechaTimbrado.getTime();
          java.text.SimpleDateFormat sdf1=new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
          String vfechaTimbrado = sdf1.format(vFechaTimbradoW);
          System.out.println("Fecha timbrado Real: " + vfechaTimbrado );
          // TERMINA CODIGO QUE MANDA A TIMBRAR EL XML

          // IF TIMBRADO
          // INICIA CODIGO PARA AGREGAR LOS DATOS DE TIMBRADO AL XML.
          SAXBuilder builder2 = new SAXBuilder ();
          doc = builder2.build (new FileInputStream (strDireccion+"Nomina"+data[0].folio+".xml"));
          Element raiz2 = doc.getRootElement();
          java.util.List<Element> elementos=raiz2.getChildren();
          Iterator i = elementos.iterator();
         // WHILE
         
         while (i.hasNext()) {
           Element e2 = (Element) i.next();
           String texto = e2.getName();
          if(texto.equals("Complemento")){
               Element item1 = new Element("TimbreFiscalDigital", "tfd","http://www.sat.gob.mx/TimbreFiscalDigital");
              item1.setAttribute("FechaTimbrado", vfechaTimbrado);
              item1.setAttribute("UUID", cert.getResultados()[0].getUuid());
              item1.setAttribute("noCertificadoSAT", cert.getResultados()[0].getCertificadoSAT());
              item1.setAttribute("selloCFD", SelloDig.replaceAll("[\n\r]",""));
              item1.setAttribute("selloSAT", cert.getResultados()[0].getSelloSAT().replaceAll("[\n\r]",""));
              item1.setAttribute("version", cert.getResultados()[0].getVersionTFD());
              Namespace XSI = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                        e2.addNamespaceDeclaration(XSI);
                        item1.setAttribute("schemaLocation", "http://www.sat.gob.mx/TimbreFiscalDigital http://www.sat.gob.mx/sitio_internet/timbrefiscaldigital/TimbreFiscalDigital.xsdxsi", XSI);

            e2.addContent(item1);
 
             XMLOutputter outputter2 = new XMLOutputter();
             FileWriter writer2 = new FileWriter(strDireccion+"Nomina"+data[0].folio+".xml");
             outputter2.output(doc, writer2);
             writer2.close();   

             /********Gurdado de datos*******/
             String path="DEBECB0C59D34B3C80F5578FB622AB14/"+r;
              Connection conn = null;
              try {
              System.out.println("Realizando el guardado de informacion");
              conn = this.getTransactionConnection();
              RnigData.actu(conn, this,vfechaTimbrado,cert.getResultados()[0].getSelloSAT().replaceAll("[\n\r]",""),SelloDig.replaceAll("[\n\r]",""),cert.getResultados()[0].getUuid(),
              cert.getResultados()[0].getCertificadoSAT(),cert.getResultados()[0].getVersionTFD(),strCadenaOriginal,id);
              System.out.println("Paso el agregado");
              RnigData.ins(conn, this,vars.getClient(), vars.getOrg(), vars.getUser(),vars.getUser(),
              "Nomina"+data[0].folio+".xml","Nomina"+data[0].folio+".xml","DEBECB0C59D34B3C80F5578FB622AB14", id,
              path);
              System.out.println("Termino el guardado de informacion");
              releaseCommitConnection(conn);

              myMessage.setType("Success");
              myMessage.setTitle("Timbrado de nomina completado");
              myMessage.setMessage(Utility.messageBD(this, "Success",vars.getLanguage()));

              } catch (Exception e) {
              try {
              releaseRollbackConnection(conn);
              } catch (Exception ignored) {}
              e.printStackTrace();
              log4j.warn("Rollback in transaction");
              myMessage = Utility.translateError(this, vars, vars.getLanguage(), "ProcessRunError");
              System.out.println("Error MyMessage" + myMessage);
              }
           
            }
         }
         }else{
         System.out.println("fallo y entro al else");
         myMessage.setMessage(cert.getResultados()[0].getMensaje().toString());
         myMessage.setType("Error");
         myMessage.setTitle(Utility.messageBD(new DalConnectionProvider(), "Error en el recibo "+data[0].folio, OBContext.getOBContext().getLanguage().getLanguage()));
         return myMessage;   
         }
         
          }catch(Exception eee){
          if(strErrorE.toString().equals("")){ 
         myMessage.setMessage(strErrorE.toString());
          }else{
        myMessage.setMessage(eee.toString());
          }
         myMessage.setType("Error");
         myMessage.setTitle(Utility.messageBD(new DalConnectionProvider(), "Error en el recibo "+data[0].folio, OBContext.getOBContext().getLanguage().getLanguage()));
          }           
          }catch(Exception ee){
             System.out.println("Error");
          } 
           } catch (IOException e) {
        e.printStackTrace();
       // i.setProcessNow(false);
        myMessage.setMessage(Utility.messageBD(this, "FileCannotCreate", vars.getLanguage()));
        return myMessage;
      } 
          }//llave del for

      //Catch cierra el try de inicio
      }catch (ServletException e) {
        e.printStackTrace();
        myMessage.setMessage(Utility.messageBD(this, "FileCannotCreate", vars.getLanguage()));
        return myMessage;
      } 
    return myMessage;
  }







}

