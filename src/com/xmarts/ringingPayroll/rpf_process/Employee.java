	/************************************************************************************ 
 * Copyright (C) 2009 Openbravo S.L.U. 
 * Licensed under the Openbravo Commercial License version 1.0 
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html 
 ************************************************************************************/

package com.xmarts.ringingPayroll.rpf_process;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import java.lang.Exception; 

import java.io.*;
import java.io.File;
import java.io.FileInputStream;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.security.PrivateKey;
import java.security.Signature;
import org.apache.commons.ssl.PKCS8Key;
import java.io.BufferedInputStream;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.security.GeneralSecurityException; 

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Connection;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.xmlEngine.XmlDocument;

import jxl.*; 
import java.io.*; 
import com.xmarts.ringingPayroll.RPFUploadEmp;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.ad.access.User;


public class Employee extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  //private FileGeneration fileGeneration = new FileGeneration();

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Posted: doPost");

    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strKey = vars.getGlobalVariable("inprpfUploadEmpId", "Employee|RPF_Upload_Emp_ID");
      System.out.println("Clave Default:" + strKey);
      String strWindowId = vars.getGlobalVariable("inpwindowId", "Employee|windowId");
      String strTabId = vars.getGlobalVariable("inpTabId", "Employee|tabId");
      String strProcessId = vars.getGlobalVariable("inpProcessed", "Employee|processId", "");
      String strPath = vars.getGlobalVariable("inpPath", "Employee|path", strDireccion
          + request.getServletPath());
      String strPosted = null;
      String strEmployeed = "N";
      String strDocNo = null;
      String strWindowPath = Utility.getTabURL(this, strTabId, "E");
      if (strWindowPath.equals(""))
        strWindowPath = strDefaultServlet; 

      if (strEmployeed.equals("Y"))
	advisePopUp(request, response, Utility.messageBD(this, "ERROR", vars.getLanguage()),
	    Utility.messageBD(this, "CFDI_Employee", vars.getLanguage()));

      printPage(response, vars, strKey, strWindowId, strTabId, strProcessId, strPath, strPosted,
          strWindowPath, strDocNo);

    } else if (vars.commandIn("SAVE")) {

      String strKey = vars.getGlobalVariable("inprpfUploadEmpId", "Employee|RPF_Upload_Emp_ID");
      String strWindowId = vars.getRequestGlobalVariable("inpwindowId", "Employee|windowId");
      String strTabId = vars.getRequestGlobalVariable("inpTabId", "Employee|tabId");
      String strProcessId = vars.getRequestGlobalVariable("inpProcessed", "Employee|processId");
      String strPath = vars.getRequestGlobalVariable("inpPath", "Employee|path");
      String strPosted = null;
      String strDocNo = null;
      String strWindowPath = Utility.getTabURL(this, strTabId, "E");
      String strFilePath = vars.getRequestGlobalVariable("inpFilePath", "Employee|FileName");

      if (strWindowPath.equals(""))
        strWindowPath = strDefaultServlet;

        OBError myMessage = process(response, vars, strKey, strWindowId, strTabId, strProcessId,
        strPath, strPosted, strWindowPath, strDocNo);
        vars.setMessage(strTabId, myMessage);
	printPageClosePopUp(response, vars, strWindowPath);
     
    } else {
      advisePopUp(request, response, Utility.messageBD(this, "ERROR", vars.getLanguage()), Utility
          .messageBD(this, "ERROR", vars.getLanguage()));
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId, String strPath, String strPosted,
      String strFilePath, String strDocNo) throws IOException, ServletException {
    if (log4j.isDebugEnabled())
      log4j.debug("Output: Button process Employee Electronic Invoice");
//System.out.println("metodo prong");
    String[] discard = { "" };
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("com/xmarts/ringingPayroll/rpf_process/Employee", discard).createXmlDocument();
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("tab", strTab);
    xmlDocument.setParameter("processed", strProcessId);
    xmlDocument.setParameter("path", strPath);
    xmlDocument.setParameter("posted", strPosted);
    xmlDocument.setParameter("docNo", strDocNo);

    {
      OBError myMessage = vars.getMessage("Employee");
      vars.removeMessage("Employee");
      if (myMessage != null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }

    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("cInvoiceId", strKey);

    if (!strFilePath.equals("")) {
      xmlDocument.setParameter("inpFilePath", strFilePath);
      xmlDocument.setParameter("inpOnLoad", "onLoadClose();");
    }
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private OBError process(HttpServletResponse response, VariablesSecureApp vars, String strKey,String windowId, String strTab, String strProcessId, String strPath, String strPosted,
      String strWindowPath, String strDocNo) {
     OBError myMessage = new OBError();
     myMessage.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
      System.out.println("entro al metodo" + strKey);
      String g=strKey;
      String r=null;
      for(int x=2;x<g.length();x=x+3){
           if(x==2){
           r=""+g.charAt(x-2)+g.charAt(x-1)+g.charAt(x);   
           }else{
           r=r+"/"+g.charAt(x-2)+g.charAt(x-1)+g.charAt(x);
           }
           System.out.println(x);
           if(x==29){
           r=r+"/"+g.charAt(x+1)+g.charAt(x+2)+"/";   
           }
       }
      System.out.println("Ruta del id divido "+r);
        
      String strDireccion = globalParameters.strFTPDirectory+"/"+"06D7B5E8C9D54AB28DA8CBD95D30F833"+"/"+r;
      
      System.out.println("Direccion del attachments "+strDireccion);

      try{

      String archivo = UploadData.file(new DalConnectionProvider(),strKey);
      if(archivo.equals("")){
       myMessage.setMessage("Es necesario adjuntar el archivo de Excel");
       myMessage.setType("Error");
       myMessage.setTitle(Utility.messageBD(new DalConnectionProvider(), "Error", OBContext.getOBContext().getLanguage().getLanguage()));
        return myMessage;   
      }
      String archivoDestino = strDireccion+archivo;
      System.out.println("Nombre del archivo "+archivoDestino);

      RPFUploadEmp rpfcon = OBDal.getInstance().get(RPFUploadEmp.class, strKey);

      Client client = rpfcon.getClient();
      String clien = client.getId();
      System.out.println("Client "+clien);

      Organization organiz = rpfcon.getOrganization();
      String org = organiz.getId();
      System.out.println("Organization "+org);

      User user = rpfcon.getCreatedBy();
      String use = user.getId();
      System.out.println("User "+use);

      try { 
            Workbook archivoExcel = Workbook.getWorkbook(new File( 
            archivoDestino)); 
            System.out.println("Número de Hojas\t" + archivoExcel.getNumberOfSheets()); 
                for (int sheetNo = 0; sheetNo < archivoExcel.getNumberOfSheets(); sheetNo++) // Recorre                                                                                                                                                       
                { 
                    Sheet hoja = archivoExcel.getSheet(sheetNo); 

                            int numFilas = hoja.getRows(); 
                            System.out.println("Filas "+numFilas);
                            String data; 
                            System.out.println("Nombre de la Hoja\t"+ archivoExcel.getSheet(sheetNo).getName()); 
                            UploadData.elim(this);
                            System.out.println("Borrando lineas antes cargardas");
                            for (int fila = 1; fila <= numFilas; fila++) { // Recorre cada 
                            // fila de la  hoja 
                                    String nom=hoja.getCell("A"+fila).getContents();
                                    String rfc=hoja.getCell("B"+fila).getContents();
                                    String calle=hoja.getCell("C"+fila).getContents();
                                    String noext=hoja.getCell("D"+fila).getContents();
                                    String noint=hoja.getCell("E"+fila).getContents();
                                    String col=hoja.getCell("F"+fila).getContents();
                                    String loc=hoja.getCell("G"+fila).getContents();
                                    String mun=hoja.getCell("H"+fila).getContents();
                                    String est=hoja.getCell("I"+fila).getContents();
                                    String pais=hoja.getCell("J"+fila).getContents();
                                    String cp=hoja.getCell("K"+fila).getContents();
                                    String regis=hoja.getCell("L"+fila).getContents();
                                    String num=hoja.getCell("M"+fila).getContents();
                                    String curp=hoja.getCell("N"+fila).getContents();
                                    String tiporeg=hoja.getCell("O"+fila).getContents();
                                    String numseg=hoja.getCell("P"+fila).getContents();
                                    String depa=hoja.getCell("Q"+fila).getContents();
                                    String banco=hoja.getCell("R"+fila).getContents();
                                    String fechaini=hoja.getCell("S"+fila).getContents();
                                    String antigue=hoja.getCell("T"+fila).getContents();
                                    String puesto=hoja.getCell("U"+fila).getContents();
                                    String tipocon=hoja.getCell("V"+fila).getContents();
                                    String tipojor=hoja.getCell("W"+fila).getContents();
                                    String salariob=hoja.getCell("X"+fila).getContents();
                                    String riesgo=hoja.getCell("Y"+fila).getContents();
                                    String salariod=hoja.getCell("Z"+fila).getContents();
                                    String met=hoja.getCell("AA"+fila).getContents();
                                    String cond=hoja.getCell("AB"+fila).getContents();
                                    String mon=hoja.getCell("AC"+fila).getContents();
                                    String forma=hoja.getCell("AD"+fila).getContents();
                                    String numcuenta=hoja.getCell("AE"+fila).getContents();
                                    
                                  if(nom.equals("") && rfc.equals("") && calle.equals("")){
                                    System.out.println("entro al break");
                                    break;
                                  }else{       
                                    //System.out.println(num+" "+con+" "+percep+" "+deducci+" "+dias+"\n"); 
                                    if(nom.equals("NombreEmpleado") && rfc.equals("RFC") && calle.equals("Calle")){
                                      System.out.println("entro al encabezado :p");
                                    }else{
                                    System.out.println("Guardando datos");
                                    UploadData.uplo(this,clien,org,use,use,nom,rfc,calle,noext,noint,col,loc,mun,est,pais,cp,num,curp,tiporeg,numseg,depa,banco,fechaini,antigue,puesto,tipocon,tipojor,salariob,riesgo,salariod,met,cond,mon,forma,numcuenta,regis);
                                    System.out.println("Termino de guardar la informacion");
                                    }
                                  }
                            }
                }
            } catch (Exception ioe) { 
              System.out.println("Entro al try exception");
              myMessage.setMessage(ioe.getMessage());
              myMessage.setType("Error");
              myMessage.setTitle(Utility.messageBD(new DalConnectionProvider(), "Error", OBContext.getOBContext().getLanguage().getLanguage()));
          return myMessage;
            } 
      }catch (ServletException e) {
        System.out.println("Entro al try Servelet exception");
        myMessage.setMessage(e.getMessage());
        myMessage.setType("Error");
        myMessage.setTitle(Utility.messageBD(new DalConnectionProvider(), "Error", OBContext.getOBContext().getLanguage().getLanguage()));
          return myMessage; 
      }
   myMessage.setMessage("El excel fue cargado exitosamente.");
   myMessage.setType("Success");
   myMessage.setTitle(Utility.messageBD(new DalConnectionProvider(), "Success", OBContext.getOBContext().getLanguage().getLanguage()));

    return myMessage;
  }







}

